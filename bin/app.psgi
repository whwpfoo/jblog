use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use JBlog;

sub main { JBlog -> to_app; }

main();