import { ajax, validate, decodeHtml } from './common.js';
import './external/simplemde.min.js';
import './external/marked.min.js';

!function() {
    var simplemde;

    init();

    var isUpdate = document.querySelector("#updateCheck").value;

    if (isUpdate === '1')
        setUpdate();


    document.querySelector('.CodeMirror').addEventListener('drop', upload);

    function init() {
        simplemde = new SimpleMDE({
            element: document.getElementById("content"),
            autosave: {
                enabled: false,
            },
            status: false,
            spellChecker: false,
            styleSelectedText: false,
            indentWithTabs: false,
            autofocus: true,
            lineWrapping: true,
            hideIcons: ['guide'],
            tabSize: 4,
            previewRender: function(plainText) {
                return marked(plainText); // Returns HTML from a custom parser
            },
        });
        
        document.querySelector('#update').style.display = 'none';
        document.querySelector('#inputTag').addEventListener('keyup', addTag);
        document.querySelector('#regist').addEventListener('click', write);
        document.querySelector('#cancel').addEventListener('click', cancel);
    }

    function setUpdate() {
        var postId = document.querySelector("#postId").value;

        ajax('get', `/posts/${postId}`, null, 'json', function(res) {
            var result = JSON.parse(res.response);
            
            if (!result) {
                alert('등록/수정 중 오류가 발생했습니다');
                location.href = '/';
                return;
            }

            var post = result.post;
            var tags = post.tags;
            var inputTag = document.querySelector("#inputTag");
            
            document.querySelector("#title").value = decodeHtml(post.title);
            
            tags.forEach(tag => {
                var tagElement = document.createElement('div');
                var newValue = document.createTextNode(decodeHtml(tag.tag));
                
                tagElement.appendChild(newValue);
                tagElement.dataset.id = tag.id;
                tagElement.dataset.post_id = tag.post_id;
                tagElement.dataset.txt = tag.tag;

                tagElement.addEventListener('click', removeTag);
                document.querySelector('.tags').insertBefore(tagElement, inputTag);
            });

            simplemde.value(post.content);
            document.querySelector("#regist").style.display = 'none';
            document.querySelector("#update").style.display = 'inline-block';
            document.querySelector("#update").addEventListener('click', update);
        });
    }

    function write(e) {
        var title = document.querySelector("#title").value;

        if (validate({ name: "제목" }, title))
            return;

        var content = simplemde.value();
        var content_html = simplemde.options.previewRender(content);
        var tags = [];

        document.querySelectorAll(".tags div").forEach(node => tags.push(node.dataset.txt));

        var data = {
            title: title,
            tags: tags,
            content: content,
            content_html: content_html 
        };

        submit(data, 'I');
    }

    function update(e) {
        var title = document.querySelector("#title").value;
        var postId = document.querySelector("#postId").value;

        if (validate({ name: "제목" }, title))
            return;

        var content = simplemde.value();
        var content_html = simplemde.options.previewRender(content);
        var updateTags = {};
        var addTags = [];

        document.querySelectorAll(".tags div").forEach(node => {
            var tagId = node.dataset.id ?? '';
            var tag = node.dataset.txt;

            if (tagId) {
                var currTag = {
                    id: tagId,
                    tag: tag,
                    post_id: postId,
                }
                updateTags[`${tagId}`] = currTag;
            } else {
                addTags.push(tag);
            }
        });

        var data = {
            id : postId,
            title : title,
            add : addTags,
            update : updateTags,
            content : content,
            content_html : content_html
        };

        submit(data, 'U');
    }

    function submit(data, type) {
        var method = type === 'I' ? 'post' : 'put';

        ajax(method, '/write', data, 'json', function(res) {
            var result = JSON.parse(res.response);
            var source = result.data;

            if (source) {
                location.href = `/${source}`;
                return;
            }

            alert('등록/수정 중 오류가 발생했습니다');
        });
    }

    function addTag(e) {
        if (e.code === 'Enter') {
            var inputTag = document.querySelector('#inputTag');
            var inputValue = inputTag.value.replace(/[\s]/gi, '');

            if (!inputValue)
                return;
            
            // 태그 중복 검사
            var tags = document.querySelectorAll('.tags div');
            for (var i = 0; i < tags.length; i++) {
                var value = tags[i].dataset.txt;
                if (value === inputValue) {
                    inputTag.value = '';
                    return;
                }
            }

            var newTag = document.createElement('div');
            var newValue = document.createTextNode(inputValue);
            
            newTag.dataset.txt = inputValue;
            newTag.appendChild(newValue);
            newTag.addEventListener('click', removeTag);
            document.querySelector('.tags').insertBefore(newTag, inputTag);

            inputTag.value = '';
        }
    }

    function removeTag(e) {
        e.target.remove();
        return;
    }

    function cancel() {
        history.go(-1);
    }

    function upload(e) {
        e.preventDefault();

        var file = e.dataTransfer.files[0];
        var types = file.type.split("/");
        var size = file.size;

        if (types[0] !== 'image' || size <= 0)
            return false;

        var formData = new FormData();
        var method = 'post';

        formData.append('image', file);

        ajax(method, '/upload', formData, null, function(res) {
            var filename = res.response;

            if (!filename) {
                alert('이미지 업로드 실패하였습니다.');
                return false;
            }

            var content = simplemde.value();
            content += `![](/images/posts/${filename})`;
            simplemde.value(content);
        });
    }
}();
