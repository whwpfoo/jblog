function ajax(method, url, param, contentType, callback) {
    var xhr;

    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                callback(xhr);
            } else {
                alert('실행 중 오류가 발생했습니다');
            }
        }
    }

    xhr.open(method, url, true);
    if (contentType === 'json') {
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        param = JSON.stringify(param);
    } else if (contentType === 'text') {
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); // ajax 임을 알리는 커스텀 헤더
    xhr.send(param);
}

function validate({ name }, ...args) {
    return args.some(value => {
        if (value === undefined || value === '') {
            alert(`${name} 을 입력해주세요`);
            return true;
        }
    });
}

function decodeHtml(htmlString) {
    var tearea = document.createElement('textarea');
    tearea.innerHTML = htmlString;
    return tearea.childNodes.length === 0 ? "" : tearea.childNodes[0].nodeValue;
}

function removeHtmlTag(htmlString) {
    var value = htmlString;
    value = value.replace(/(<([^>]+)>)/ig, "");
    return value;
}

function setRecaptchaToken(callback, target, password) {
    grecaptcha.ready(function() {
        var siteKey = document.querySelector('#siteKey').value;
        grecaptcha.execute(siteKey, {action: 'homepage'}).then(function(token) {
            document.querySelector('#grecaptcha').value = token;
            if (target) {
                callback(target, password);
            } else {
                callback();
            }
        });
    });
}

export { ajax, validate, decodeHtml, removeHtmlTag, setRecaptchaToken };
