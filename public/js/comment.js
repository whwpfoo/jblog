import { ajax, validate, decodeHtml, removeHtmlTag, setRecaptchaToken } from './common.js';

!function() {
    document.querySelectorAll('.comment-textarea').forEach(ele => ele.addEventListener('keyup', resize));
    document.querySelectorAll('.comment-add-btn').forEach(ele => ele.addEventListener('click', addComment));
    document.querySelectorAll('.modify-comment').forEach(ele => ele.addEventListener('click', checkGuest));
    document.querySelectorAll('.delete-comment').forEach(ele => ele.addEventListener('click', checkGuest));
    document.querySelectorAll('.cancel-btn').forEach(btn => btn.addEventListener('click', cancel));
    document.querySelectorAll('.modify-btn').forEach(btn => btn.addEventListener('click', modify));
    document.querySelectorAll('.guest-reply').forEach(reply => reply.addEventListener('click', replyList));
    document.querySelectorAll('.reply-btn').forEach(btn => btn.addEventListener('click', function() {
        event.target.classList.toggle('d-none');
        event.target.nextElementSibling.classList.toggle('d-none');
    }));
    document.querySelectorAll('.comment-cancel-btn').forEach(btn => btn.addEventListener('click', function() {
        event.target.closest('.comment-input').classList.toggle('d-none');
        event.target.closest('.comment-input').previousElementSibling.classList.toggle('d-none');
    }));

    function replyList() {
        var replyEle = event.target;
        var commentId = replyEle.dataset.id;

        var label = replyEle.innerText;
        if (label === '숨기기') {
            var count = replyEle.dataset.count;
            if (count) {
                replyEle.innerText = `${count}개의 답글`;
            } else {
                replyEle.innerText = '답글 달기';
            }
        } else {
            replyEle.innerText = '숨기기';
        }

        if (replyEle.dataset.init) {
            replyEle.nextElementSibling.classList.toggle('d-none');
            return;
        }

        ajax('get', `/comments/reply/${commentId}`, null, 'json', function(res) {
            var data = JSON.parse(res.response);
            var result = data.success;
            var reply = data.replys;

            if (result) {
                var replyContainer = replyEle.nextElementSibling;
                var replyList = replyContainer.querySelector('.reply-list');
                var init = document.querySelectorAll(`.pcomment-${commentId}`).length == 0 ? true : false;
                reply.forEach((r, index) => {
                    replyList.insertAdjacentHTML('beforeend', gridComment(r, init, postId));
                    if (r.is_delete !== 'Y') {
                        var id = r.id;
                        var commentEle = document.querySelector(`.comment-${id}`);
                        commentEle.querySelector('.comment-textarea').addEventListener('keyup', resize);
                        commentEle.querySelector('.modify-comment').addEventListener('click', checkGuest);
                        commentEle.querySelector('.delete-comment').addEventListener('click', checkGuest);
                        commentEle.querySelector('.cancel-btn').addEventListener('click', cancel);
                        commentEle.querySelector('.modify-btn').addEventListener('click', modify);
                    }
                });

                replyContainer.classList.toggle('d-none');
                if (reply.length > 0) { // 답글 존재
                    replyContainer.querySelector('.reply-btn').classList.toggle('d-none');
                } else {                // 답글 없음
                    replyContainer.querySelector('.comment-input').classList.toggle('d-none');
                }
                replyEle.dataset.init = "1";
            }
        });
    }

    function addComment() {
        var target = event.target;
        setRecaptchaToken(function(target) {
            console.log(target);
            var root = target.closest('.comment-input');
            var guestEle = root.querySelector('input[name="guest"]');
            var passEle = root.querySelector('input[name="guestPassword"]');
            var commentInputEle = root.querySelector('textarea[name="comment"]');

            var guest = guestEle.value;
            var guestPassword = passEle.value;
            var comment = commentInputEle.value;

            if (validate({name: '아이디/비밀번호 또는 댓글'}, guest, guestPassword, comment)) {
                return;
            }

            var postId = document.querySelector('#postId').value;
            var parentId = target.dataset.parent;
            var grecaptchaToken = document.querySelector('#grecaptcha').value;

            var param = {
                guest           : guest,
                guest_password  : guestPassword,
                comment         : comment,
                parent_id       : parentId,
                grecaptcha_token: grecaptchaToken
            };

            ajax('post', `/comments/${postId}`, param, 'json', function(res) {
                var data = JSON.parse(res.response);
                var result = data.success;

                if (result) {
                    var comments = data.comments;
                    var commentLength = comments.length;

                    comments.forEach((comment, idx) => {

                        var id = comment.id; // 신규로 추가된 코멘트 아이디
                        var element = document.querySelector(`.comment-${id}`);
                        if (element) {
                            return;
                        } // 이미 존재하는 코멘트는 생략

                        var parentId = comment.parent_id ?? ''; // 댓글인지 대댓글인지 판단하기 위한 값
                        var init = document.querySelectorAll(`.pcomment-${parentId}`).length == 0 ? true : false;
                        var container = parentId ? root.parentElement.querySelector('.reply-list')
                                                 : document.querySelector('.comments-list');

                        container.insertAdjacentHTML('beforeend', gridComment(comment, init, parentId));

                        addCommentCount();
                        var commentEle = document.querySelector(`.comment-${id}`);
                        commentEle.querySelector('.comment-textarea').addEventListener('keyup', resize);
                        commentEle.querySelector('.modify-comment').addEventListener('click', checkGuest);
                        commentEle.querySelector('.delete-comment').addEventListener('click', checkGuest);
                        commentEle.querySelector('.cancel-btn').addEventListener('click', cancel);
                        commentEle.querySelector('.modify-btn').addEventListener('click', modify);

                        if (!parentId) {
                            commentEle.querySelector('.guest-reply').addEventListener('click', replyList);
                            commentEle.querySelector('.comment-add-btn').addEventListener('click', addComment);
                            commentEle.querySelector('.reply-btn').addEventListener('click', function() {
                                event.target.classList.toggle('d-none');
                                event.target.nextElementSibling.classList.toggle('d-none');
                            });
                            commentEle.querySelector('.comment-cancel-btn').addEventListener('click', function() {
                                event.target.closest('.comment-input').classList.toggle('d-none');
                                event.target.closest('.comment-input').previousElementSibling.classList.toggle('d-none');
                            });
                        }
                    });

                    // 아이디/패스워드/코멘트 창 초기화
                    guestEle.value = '';
                    passEle.value = '';
                    commentInputEle.value = '';
                    return;
                }
                alert('댓글 추가가 실패하였습니다.');
            });
        }, target);
    }

    function remove(target, password) {
        setRecaptchaToken(function(target, password) {
            var commentId = target.parentElement.parentElement.dataset.id;
            var grecaptchaToken = document.querySelector('#grecaptcha').value;

            var param = {
                id: commentId,
                is_delete: 'Y',
                guest_password : password,
                grecaptcha_token: grecaptchaToken
            };

            ajax('put', `/comments/${commentId}`, param, 'json', function(res) {
                var data = JSON.parse(res.response);
                var result = data.success;
                if (result) {
                    var message = data.comment;
                    target.parentElement.nextElementSibling.firstElementChild.innerText = message;
                    target.closest('.guest-actions').remove();
                } else {
                    alert('삭제 실패하였습니다.');
                    return;
               }
            });
        }, target, password);
    }

    function showModifyForm(target, password) {
        var commentArea = target.parentNode.nextElementSibling; // guest-content
        var currentCommentEle = commentArea.firstElementChild;  // crrunt comment value
        var commentInputArea = commentArea.lastElementChild;    // textarea parent

        // 기존 코멘트 영역 숨김
        var currentText = currentCommentEle.innerText;
        currentCommentEle.classList.toggle('d-none');

        // 수정 코멘트 입력 창 노출
        commentInputArea.classList.toggle('d-none');
        commentInputArea.firstElementChild.value = currentText;
        commentInputArea.firstElementChild.dataset.password = password;

        // 액션 (수정/삭제) 영역 숨김
        commentArea.previousElementSibling.classList.toggle('d-none');
    }

    function modify() {
        var target = event.target;
        setRecaptchaToken(function(target) {
            var guestContentEle = target.closest('.guest-content');
            var commentId = guestContentEle.parentElement.dataset.id;
            var commentInputText = guestContentEle.lastElementChild.firstElementChild.value;
            var password = guestContentEle.lastElementChild.firstElementChild.dataset.password;
            var grecaptchaToken = document.querySelector('#grecaptcha').value;

            var param = {
                id: commentId,
                comment: commentInputText,
                guest_password: password,
                grecaptcha_token: grecaptchaToken
            };

            ajax('put', `/comments/${commentId}`, param, 'json', function(res) {
                var data = JSON.parse(res.response);
                var result = data.success;
                if (result) {
                    var message = data.comment;
                    guestContentEle.firstElementChild.innerText = message;
                    guestContentEle.firstElementChild.classList.toggle('d-none');
                    guestContentEle.previousElementSibling.classList.toggle('d-none');
                    guestContentEle.lastElementChild.firstElementChild.value = '';
                    guestContentEle.lastElementChild.classList.toggle('d-none');
                }
            });
        }, target);
    }

    function cancel() {
        // guest-actions 노출
        // guest-content 의 기존 코멘트 창 노출
        // guest-content 의 코멘트 수정 영역 값 초기화 후 숨김 처리
        var guestContentEle = event.target.closest('.guest-content');
        guestContentEle.previousElementSibling.classList.toggle('d-none');
        guestContentEle.firstElementChild.classList.toggle('d-none');
        guestContentEle.lastElementChild.firstElementChild.value = '';
        guestContentEle.lastElementChild.classList.toggle('d-none');
    }

    function checkGuest(type) {
        var targetElement = event.target;
        var guestPassword = prompt('비밀번호를 입력해주세요', '');
        if (!guestPassword) {
            return;
        }
        var commentId = event.target.parentElement.parentElement.dataset.id;
        var param = {
            id : commentId,
            guest_password: guestPassword
        };
        ajax('post', '/comments/guest/verifycation', param, 'json', function(res) {
            var data = JSON.parse(res.response);
            var result = data.success;
            var actionType = targetElement.dataset.type;
            if (result) {
                if (actionType === 'modify') {
                    showModifyForm(targetElement, guestPassword);
                } else {
                    remove(targetElement, guestPassword);
                }
            } else {
                var message = data.error;
                alert(message);
            }
        });
    }

    function resize() {
        var target = event.target;
        target.style.height = 'auto';
        var currentHeight = target.scrollHeight;
        target.style.height = currentHeight + 'px';
    }

    function gridComment(comment, init, existParent) {
        var date = comment.created_at.split(' ')[0].split('-');
        var dateString = `${date[0]}년 ${date[1]}월 ${date[2]}일`;
        var parentId = comment.parent_id ? comment.parent_id : '';
        return `${ existParent || init ? '' : '<hr>' }
                <div class='comment-${comment.id} pcomment-${parentId} pt-2 pb-2' data-id='${comment.id}'>
                    <div class="guest-info d-inline">
                        <span class="fs-5 fw-bold">
                            ${comment.guest}
                        </span>
                        <span class="text-muted">${dateString}</span>
                    </div>

                    ${comment.is_delete !== 'Y' ?
                        `<div class="guest-actions float-end pt-2 text-muted">
                             <span class="modify-comment" data-type="modify">수정</span>
                             <span class="delete-comment" data-type="delete">삭제</span>
                         </div>` : ''
                    }

                    <div class="guest-content pt-3 pb-4">
                        <div class="">
                            ${comment.comment}
                        </div>
                        <div class="d-none">
                            <textarea name="comment" class="comment-textarea" placeholder="댓글을 입력해주세요."></textarea>
                            <div class="text-end">
                                <button class="btn cancel-btn">취소</button>
                                <button class="btn modify-btn">댓글 수정</button>
                            </div>
                        </div>
                    </div>

                    ${ !existParent ?
                        `<div class="guest-reply" data-id='${comment.id}'>
                            답글 달기
                        </div>
                        <div class="d-none p-3">
                            <div class="reply-list"></div>
                            <button type="button" class="d-none reply-btn">답글 작성하기</button>
                            <div class="d-none comment-input">
                                <div class="input-group justify-content-end mb-3">
                                    <div class="pe-2">
                                        <input class="form-control" type="text" name="guest" placeholder="아이디">
                                    </div>
                                    <div>
                                        <input class="form-control" type="password" name="guestPassword" placeholder="비밀번호">
                                    </div>
                                </div>
                                <textarea name="comment" class="comment-textarea" placeholder="댓글을 입력해주세요."></textarea>
                                <div class="text-end">
                                    <button type="button" class="badge bg-success comment-cancel-btn">취소</button>
                                    <button type="button" class="badge bg-success comment-add-btn" data-parent='${comment.id}'>댓글 작성</button>
                                </div>
                            </div>
                        </div>
                        ` : ''
                    }
                </div>
                ${ existParent ? '<hr>' : '' }`;
    }

    function addCommentCount() {
        var ele = document.querySelector('#commentCtn');
        var totalCount = ele.dataset.count;
        totalCount++;

        ele.dataset.count = totalCount;
        ele.innerText = `${totalCount}개의 댓글`;
    }
}();
