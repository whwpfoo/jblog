import { ajax } from './common.js';

!function() {
    var contentHtml = document.querySelector('.contents input').dataset.value;
    var isLogin = document.querySelector('#logined').value;

    document.querySelector('#contentView').innerHTML = contentHtml;
    makeDocList();

    if (isLogin) {
        document.querySelector('#update').addEventListener('click', update);
        document.querySelector('#delete').addEventListener('click', remove);
    }

    window.addEventListener('scroll', function() {
        var scrollHeight = window.scrollY;
        var container = document.querySelector('.doc-list');
        if (scrollHeight <= 200) {
            container.style.position = 'relative';
            container.style.top = null;
            return;
        }
        container.style.position = 'fixed';
        container.style.top = '114px';
    });



    function makeDocList() {
        var container = document.querySelector('.doc-list');
        var list = document.querySelectorAll('h3[id]');

        list.forEach((ele, idx) => {
            var id = ele.id;
            var text = ele.innerText;
            var head = `<div ${idx != 0 ? 'class="mt-1"' : 'class=""'}><a href="#${ele.id}">${ele.innerText}</a></div>`;

            container.insertAdjacentHTML('beforeend', head);
        });
    }

    function update() {
        var postId = document.querySelector('#postId')?.value;
        location.href = `/write?id=${postId}`;
    }

    function remove() {
        var postId = document.querySelector('#postId')?.value;
        if (confirm('해당 게시물을 삭제하시겠습니까?')) {
            var url = `/delete/${postId}`;

            ajax('delete', url, null, 'json', function(res) {
                var data = JSON.parse(res.response);
                var result = data.success;
                if (result) {
                    alert('삭제 되었습니다');
                    location.href = '/';
                    return;
                } else {
                    alert('삭제에 실패하였습니다.');
                }
            });
        }
    }
}();
