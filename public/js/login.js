import { ajax, validate, setRecaptchaToken } from './common.js';

!function() {

    document.querySelector('#loginBtn').addEventListener('click', login);

    function login() {
        setRecaptchaToken(function() {
            var email = document.querySelector('#email').value;
            var password = document.querySelector('#password').value;
            var returnUrl = document.querySelector('#returnUrl').value;
            var grecaptchaToken = document.querySelector('#grecaptcha').value;

            if (validate({ name: "아이디 또는 비밀번호" }, email, password))
                return;

            var data = {
                email           : email,
                password        : password,
                return_url      : returnUrl,
                grecaptcha_token: grecaptchaToken
            };
            submit(data);
        });
    }

    function submit(data) {
        ajax('post', '/user/login', data, 'json', function(res) {
            var data = JSON.parse(res.response);
            var result = data.success;

            if (result) {
                var returnUrl = data.return_rul;
                if (returnUrl) {
                    location.href = returnUrl;
                } else {
                    location.href = '/';
                }
            } else {
                var message = data.error;
                alert(message);
            }
        });
    }
}();
