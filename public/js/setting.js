import { ajax, validate } from './common.js';


!function() {
    document.querySelector('#modiDescription').addEventListener('click', modify);
    document.querySelector('#modiTitle').addEventListener('click', modify);
    document.querySelector('#modiSocial').addEventListener('click', modify);
    document.querySelector('#modiPassword').addEventListener('click', modify);

    function modify(e) {
        var row = e.target.closest('.row');
        Array.prototype.forEach.call(row.children, (ele, index) => {
            ele.classList.toggle('d-none');
        });

        // // 수정 저장 버튼 생성 및 이벤트 등록
        var modifyButtonWrapper = document.createElement('div');
        modifyButtonWrapper.style.margin = '10px 0 10px 0';
        modifyButtonWrapper.classList.toggle('text-end');

        var modifyButton = document.createElement('button');
        modifyButton.innerText = '저장';
        modifyButton.classList.toggle('btn');
        modifyButton.classList.toggle('btn-success');
        
        modifyButton.addEventListener('click', submit);

        modifyButtonWrapper.append(modifyButton);
        row.append(modifyButtonWrapper);
    }

    function submit(e) {
        var parent = e.target.closest('.row');
        var url = '/user/settings';
        var method = 'post';
        var params = {};

        parent.querySelectorAll('input').forEach(ele => {
            params[ele.dataset.key] = ele.value;
        });

        if (params.pwd) {
            var password = params.pwd;
            var passwordCheck = params.pwdCheck;

            if (password !== passwordCheck) {
                alert('입력하신 비밀번호가 일치하지 않습니다.');
                return false;
            }

            delete params.pwdCheck;
        }

        ajax(method, url, params, 'json', function(res) {
            var data = JSON.parse(res.response);
            var result = data.success;

            if (result) {
                var options = data.settings;

                // 변경된 세팅 사용자 정보로 element 값 갱신
                Object.keys(params).forEach(key => {
                    if (key === 'pwd') return;
                    parent.querySelector(`#${key}`).innerText = options[key];
                    parent.querySelectorAll(`input[data-key='${key}']`)[0].value = options[key];
                });

                e.target.remove();

                Array.prototype.forEach.call(parent.children, (ele, index) => {
                    ele.classList.toggle('d-none');
                });
                alert('변경되었습니다.');
            } else {
                alert('변경에 실패하였습니다. 다시 시도해주세요.');
            }
        });
    }
}();
