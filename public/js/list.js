import { ajax, validate, removeHtmlTag } from './common.js';

!function() {
    var list = [];
    var page = 1;
    var mutex = false;
    var scrollock = false;
    var finish = false;

    if (location.hash) {
        var data = history.state;
        if (data) {
            scrollock = true;

            var ul = document.querySelector("#posts");
            data.list.forEach(post => ul.insertAdjacentHTML('beforeend', grid(post)));
            list = list.concat(data.list);
            page = data.page;

            scrollock = false;
        }
    } else {
        getList();
    }
    

    window.addEventListener('scroll', load);
    document.querySelector('#keyword').addEventListener('keyup', search); 
    document.querySelector('#keyword').addEventListener('focus', (e) => {
        var target = e.target.closest('div');
        console.log(target);
        target.classList.add('focus');
    });

    document.querySelector('#keyword').addEventListener('blur', (e) => {
        var target = e.target.closest('div');
        target.classList.remove('focus');
    });


    function getList() {
        //key=value AND '?'
        var queryString = location.search ? `${location.search.substring(1)}` : '';
        console.log(`query string >> ${queryString}`);


        if (history.state?.query) {
            console.log(`exist history query string >> ${history.state.query}`);
            queryString = history.state.query;
        }

        var url;
        if (queryString) {
            url = `/posts?${ queryString }&page=${ page }`;
        } else {
            url = `/posts?page=${ page }`;
        }

        ajax('get', url, null, 'json', function(res) {
            var data = JSON.parse(res.response);
            var posts = data.posts;
            var ul = document.querySelector("#posts");

            if (posts.length > 0) {
                posts.forEach(post => ul.insertAdjacentHTML('beforeend', grid(post)));
                list = list.concat(posts);
                page++;
                history.replaceState({list: list, page: page, query: queryString}, 'JBlog', '/##');
            } else if (posts.length == 0 && !history.state) {
                var isEmpty = document.querySelector('#empty') ? false : true;
                if (isEmpty) {
                    ul.insertAdjacentHTML('beforeend', `<div class='list-group-item border-0 pb-4 pt-0 text-center' id='empty'>
                        작성된 글이 없습니다.</div>`
                    );
                }
            } else {
                finish = true;
            }

            scrollock = false;
            mutex = false;
        });
    }

    function load() {
        if (!scrollock) {
//            var broswerHeight = window.innerHeight;
//            var documentHeight = document.documentElement.scrollHeight;
//            var scrollHeight = window.scrollY;
            var contentHeight = document.querySelector('#posts').clientHeight;
            var screenHeight = window.screen.height;
            var cursor = window.pageYOffset;

//            if (scrollHeight >= documentHeight - broswerHeight)	{
            if ((contentHeight - screenHeight) <= cursor) {
                if (mutex || finish) {
                    return;
                }
                mutex = true;
                getList();
            }
        }
    }

    function search(e) {
        if (e.keyCode === 13) {
            var searchKeyword = document.querySelector('#keyword').value;

            if (validate({name: '검색어'}, searchKeyword)) {
                return;
            }

            location.href = `/?q=${searchKeyword}`; 
        }
    }

    function grid(data) {
        var date = data.created_at.split(' ')[0].split('-');
        var dateString = `${date[0]}년 ${date[1]}월 ${date[2]}일`;
        return `<a href=/${data.source} class='list-group-item shadow-sm border-0 border-bottom mb-5 pb-4 pt-0'>
                    <div class='d-flex w-100 justify-content-between'>
                        <h5 class='mb-1'>${data.title}</h5>
                    </div>
                    <p class='mb-1'>${removeHtmlTag(data.content)}</p>
                    ${data.tags.map(tag => {
                        return '<span class="badge bg-secondary text-white me-2">' + tag.tag + '</span>';
                    }).join('')}
                    <div class='text-black-50 mt-3'>
                        <span>${dateString}</span>·<span>${data.reply_count}개의 댓글</span>
                    </div>
                </a>`;
    }
}();
