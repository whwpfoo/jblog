use strict;
use warnings;
use utf8;

use FindBin;
use DBI;
use Encode qw/encode/;
use URI::Escape;

use constant DOMAIN => 'https://whwp0913.me/';

sub main {
    my ($host, $port, $name, $id, $pwd) = @ARGV;
    my $path = $FindBin::RealBin . "/../../public";

    unless (-d $path) {
        print "[!] ERROR not exist path... make path!\n";
        mkdir $path;
    }

    my $dbh = DBI -> connect("DBI:mysql:database=$name;host=$host;port=$port;", $id, $pwd, {
        RaiseError        => 0,
        mysql_enable_utf8 => 1
    });

    my $sth = $dbh -> prepare("select source from posts where id > 0 order by created_at asc");
    $sth -> execute();

    my $content = << "SITE_MAP";
<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
SITE_MAP

    print "content -> $content\n";

    while (my $row = $sth -> fetchrow_array()) {
        my $title = uri_escape_utf8($row);
        my $url   = << "URL";
    <url>
        <loc>@{[DOMAIN]}$title</loc>
    </url>
URL
        $content .= $url;
    }

    $content .= "</urlset>";

    open my $handler, ">", "$path/sitemap.xml" or die "file open fail cause: $!";
    print $handler $content;

    $handler -> close();
    $sth -> finish();
    $dbh -> disconnect();
}

main();
exit;