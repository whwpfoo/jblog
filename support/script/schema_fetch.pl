use strict;
use warnings;
use FindBin qw($RealBin);

sub main {
    my ($host, $id, $password) = @ARGV;

    undef // $host
          // $id
          // $password
          // die "[!] ERROR: required connection info.";


    if ($^O eq "MSWin32") {
        system("dbicdump -o dump_directory=$RealBin\\..\\..\\lib ^-o components=\"[q{InflateColumn::DateTime}]\" ^JBlog::Schema dbi:mysql:database=jjj;host=$host;port=3306 ^$id $password");
    }

    else {
        system("dbicdump -o dump_directory=$RealBin/../../lib \\-o components='[\"InflateColumn::DateTime\"]' \\JBlog::Schema 'dbi:mysql:database=jjj;host=$host;port=3306' \\$id $password");
    }
}

main();
exit;
