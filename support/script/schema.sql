CREATE TABLE IF NOT EXISTS users (
    `id` INT NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(100) NOT NULL,
    `pwd` VARCHAR(255) NOT NULL,
    `name` VARCHAR(30) NOT NULL,
    `nickname` VARCHAR(30),
    `description` VARCHAR(500),
    `git` VARCHAR(100),
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_uk_email` (`email`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS posts (
    `id` INT NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `source` VARCHAR(255) NOT NULL,
    `content` MEDIUMTEXT,
    `content_html` MEDIUMTEXT,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `user_id`  INT NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY `post_fk_user` (`user_id`) REFERENCES users (`id`) ON DELETE CASCADE,
    INDEX `post_idx_user` (`user_id`),
    INDEX `post_idx_title` (`title`),
    UNIQUE INDEX `post_unidx_source` (`source`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS tags (
    `id` INT NOT NULL AUTO_INCREMENT,
    `tag` VARCHAR(255) NOT NULL,
    `post_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY `tag_fk_post` (`post_id`) REFERENCES posts (`id`) ON DELETE CASCADE,
    UNIQUE KEY (`tag`, `post_id`),
    INDEX `tag_idx_tag` (`tag`),
    INDEX `tag_idx_post` (`post_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS comments (
    `id` INT NOT NULL AUTO_INCREMENT,
    `parent_id` INT,
    `post_id` INT NOT NULL,
    `guest` varchar(255) NOT NULL,
    `guest_password` varchar(255) NOT NULL,
    `comment` varchar(1000) NOT NULL,
    `is_delete` char(1) DEFAULT 'N',
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    FOREIGN KEY `comment_fk_post` (`post_id`) REFERENCES posts (`id`) ON DELETE CASCADE,
    INDEX `comment_idx_parent` (`parent_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
