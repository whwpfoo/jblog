package JBlog;

use utf8;
use Dancer2;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Passphrase;
use JBlog::Control::Sitemap;
use JBlog::Control::Post;
use JBlog::Control::User;
use JBlog::Control::Comment;


BEGIN {
    my $user = schema('JBlog') -> resultset('User') -> find({
        email => 'whwp0913@gmail.com'
    });

    if (defined $user) {
        set nickname    => $user -> get_column('nickname');
        set description => $user -> get_column('description');
        set git         => $user -> get_column('git');
    }

    else {
        set nickname => 'Junje Jo';
        set git      => 'https://gitlab.com/whwpfoo'
    }

    set appname => 'JBlog';

    my $use_proxy = config -> {behind_proxy};
    if ($use_proxy) {
        set behind_proxy => $use_proxy;
    }
};


hook before => sub {
    my $path    = request -> path_info;
    my $user    = session -> read('user');
    my $is_ajax = request -> is_ajax;

    if ($path =~ /\/write|\/user\/settings|\/upload/ && !$user) {
        if ($is_ajax) {
            die "[!] 접근 권한이 없습니다.";
        }
        else {
            var return_url => $path;
            redirect '/';
        }
    }
};

1;