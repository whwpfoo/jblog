use strict;
use warnings;

package JBlog::Control::User;

use utf8;
use Dancer2 appname => 'JBlog';
use Dancer2::Plugin::DBIC;
use JBlog::Service::User qw/login update/;
use JBlog::Util          qw/valid_grecaptch/;
use Try::Tiny;


prefix '/user';

post '/login' => sub {
    my %params     = %{from_json(request -> body)};
    my $token      = $params{grecaptcha_token};
    my $secret_key = config -> {grecaptcha} -> {secret_key};
    my $verify_url = config -> {grecaptcha} -> {verify_url};


    unless(valid_grecaptch($token, $verify_url, $secret_key)) {
        info "[!] failed login cause: invalid token !";
        return to_json({
            success => 0,
            error   => "토큰 검증에 실패하였습니다."}, {
            ascii   => 1
        });
    }


    my $email      = $params{email};
    my $password   = $params{password};
    my $return_url = $params{return_url};
    my $result = login(schema('JBlog'), $email, $password);

    unless ($result) {
        info "[!] failed login cause: incorrect id or password !";
        return to_json({
            success => 0,
            error   => "아이디 또는 비밀번호가 일치하지 않습니다."}, {
            ascii   => 1
        });
    }

    session -> write('user', {
        id    => $result->get_column('id'),
        email => $result -> get_column('email')
    });

    return to_json({
        success    => 1,
        return_url => $return_url
    });
};


get '/logout' => sub {
    my $session = session -> read('user');
    if (defined $session) {
        session -> delete('user');
    }
    redirect '/';
};


get '/settings' => sub {
    return template "user/settings";
};


post '/settings' => sub {
    my %params = %{from_json(request -> body)};

    my %result  = update(schema('JBlog'), %params);
    my %options = ();

    foreach my $key (keys %params) {
        if ($key eq 'pwd') {
            $options{$key} = 1;
            next;
        }

        set $key => $result{$key};
        $options{$key} = $result{$key}
    }

    return to_json({
        success  => 1,
        settings => \%options}, {
        ascii => 1
    });
};

1;
