use strict;
use warnings;

package JBlog::Control::Post;

use utf8;
use Dancer2 appname => 'JBlog';
use Dancer2::Plugin::DBIC;
use JBlog::Service::Post qw/add update delete_by_id find_by_id find_by_title list/;
use JBlog::Service::Tag  qw/cashing/;
use JBlog::Util          qw/decode_html encode_html/;
use JBlog::Service::Comment;


get '/' => sub {
    if ($JBlog::Service::Tag::updated) {
        cashing(schema('JBlog'));
    }

    my $tags        = $JBlog::Service::Tag::cashed;
    my $title       = '춘장\'s 개발 블로그';
    my $description = join ',', keys %{$tags};

    return template "post/list", {
        tags        => $tags,
        title       => $title,
        description => $description
    };
};


get '/posts' => sub {
    my %params = %{request -> query_parameters};
    my $q      = $params{q};
    my $t      = $params{t};
    my $page   = $params{page} || 1;

    my @list = list(
        schema('JBlog'),
        q    => $q,
        t    => $t,
        page => $page
    );

    my @result = ();

    foreach my $post (@list) {
        my %post_hash   = $post -> get_columns;
        my @tags        = map { {$_ -> get_columns} } $post -> tags;
        my $reply_count = scalar map { {$_ -> get_columns} } $post -> comments;

        $post_hash{content} = substr($post_hash{content}, 0, 100); # 본문의 크기가 너무 커서 사이즈 조정
        $post_hash{tags} = \@tags;
        $post_hash{reply_count} = $reply_count;

        push @result, \%post_hash;
    }

    return to_json({
        posts => \@result,
        page  => $page}, {
        ascii => 1
    });
};


get '/posts/:id' => sub {
    my $id   = route_parameters -> get('id');
    my $post = find_by_id(schema('JBlog'), $id);

    my %post_hash = $post -> get_columns;
    my @tags      = map { {$_ -> get_columns} } $post -> tags;
    $post_hash{tags} = \@tags;

    return to_json({post => \%post_hash}, {ascii => 1});
};


get '/write' => sub {
    my %params  = %{request -> query_parameters};
    my $id      = $params{id};
    my $updated = $id ? 1 : 0;

    return template "post/write", {updated => $updated, id => $id};
};


post '/write' => sub {
    my %params = %{from_json(request -> body)};

    my $title        = $params{title};
    my $content      = $params{content};
    my $content_html = $params{content_html};
    my $user_id      = session('user') -> {id};
    my $tags         = $params{tags};

    my $result = add(schema('JBlog'), $title, $content, $content_html, $user_id, $tags);

    return to_json({data => $result}, {ascii => 1});
};


put '/write' => sub {
    my %params = %{from_json(request -> body)};

    my $tags = {
        add    => $params{add},
        update => $params{update}
    };

    my $post = {
        id           => $params{id},
        title        => $params{title},
        source       => $params{title},
        content      => $params{content},
        content_html => $params{content_html},
        user_id      => session('user') -> {id}
    };

    my $result = update(schema('JBlog'), $post, $tags);

    return to_json({data => $result}, {ascii => 1});
};


get '/:title' => sub {
    my $source = request -> route_parameters -> get('title');
    my $post   = find_by_title(schema('JBlog'), $source);

    unless($post) {
        info "[!] failed get post cause: not exist post !";
        redirect "/404.html";
    }

    # meta-description
    my $title       = $post -> get_column('title');
    my $content     = $post -> get_column('content_html');
    my @tags        = map { {$_ -> get_columns} } $post -> tags;
    my $description = join ",", map { $_ -> {tag} } @tags;

    $content = decode_html($content);
    $content =~ s/(<([^>]+)>)//g;
    $content = substr($content, 0, 120);
    $description = "[" . $description . "]" . " $content";

    # comments
    my @comments            = map { {$_ -> get_columns} } $post -> comments;
    my $comment_total_count = scalar @comments;

    my %reply = ();
    foreach (@comments) {
        unless ($_ -> {parent_id}) {
            $reply{$_ -> {id}} = $_;
            next;
        }
        $reply{$_ -> {parent_id}} -> {count}++;
    }

    my @replys = sort { $a -> {id} <=> $b -> {id} } values %reply;

    return template "post/view", {
        title         => $title,
        description   => $description,
        post          => $post,
        tags          => \@tags,
        comments      => \@replys,
        comment_count => $comment_total_count
    };
};


del '/delete/:id' => sub {
    my $session = session -> read('user');

    unless ($session) {
        return to_json({
            success => 0,
            error   => "접근 권한이 없습니다."}, {
            ascii   => 1
        });
    }

    my $delete_id = request -> route_parameters -> get('id');

    delete_by_id(schema('JBlog'), $delete_id);
    return to_json({success => 1});
};


post '/upload' => sub {
    my $upload_file = request -> upload('image');
    my $dir         = path(config -> {public_dir}, 'images', 'posts');

    mkdir $dir if not -e $dir;

    my $current  = time();
    my @type     = split '\.', $upload_file -> filename;

    my $filename = $current . '.' . $type[1];
    my $path     = path($dir, $filename);

    $upload_file -> copy_to($path);
    return $filename;
};

1;