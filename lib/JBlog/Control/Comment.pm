use strict;
use warnings;

package JBlog::Control::Comment;

use utf8;
use Dancer2 appname => 'JBlog';
use Dancer2::Plugin::DBIC;
use JBlog::Service::Comment qw/add verify_password update get_reply/;


prefix '/comments';

post '/:post_id' => sub {
    my %params    = %{from_json(request -> body)};
    my $post_id = request -> route_parameters -> get("post_id");

    $params{post_id} = $post_id;

    my $verify_url = config -> {grecaptcha} -> {verify_url};
    my $secret_key = config -> {grecaptcha} -> {secret_key};

    my @result   = add(schema('JBlog'), \%params, $verify_url, $secret_key);
    my @comments = ();

    foreach (@result) {
        my %comment = $_ -> get_columns;
        delete $comment{guest_password};
        push @comments, \%comment;
    }

    return to_json({success => 1, comments => \@comments}, {ascii => 1});
};


post '/guest/verifycation' => sub {
    my %params = %{from_json(request -> body)};

    my $result = verify_password(schema('JBlog'), \%params);
    unless ($result) {
        return to_json({success => 0, error => "비밀번호가 올바르지 않습니다."}, {ascii => 1});
    }
    return to_json({success => 1});
};


put '/:id' => sub {
    my %params = %{from_json(request -> body)};

    my $verify_url = config -> {grecaptcha} -> {verify_url};
    my $secret_key = config -> {grecaptcha} -> {secret_key};

    my %result = update(schema('JBlog'), \%params, $verify_url, $secret_key);
    return to_json({
        success => 1,
        comment => $result{comment}}, {
        ascii   => 1
    });
};


get '/reply/:parent_id' => sub {
    my $parent_id = request -> route_parameters -> get("parent_id");
    my @result = get_reply(schema('JBlog'), $parent_id);
    return to_json({success => 1, replys => \@result}, {ascii => 1});
};

1;
