use strict;
use warnings;

package JBlog::Control::Sitemap;

use utf8;
use Dancer2 appname => 'JBlog';
use Dancer2::Plugin::DBIC;

get '/sitemap.xml' => sub {
    response -> content_type("text/xml;charset=utf-8");


    my $schema = schema('JBlog');
    my @posts  = $schema -> resultset('Post') -> search({
        id       => {'>' => 0}}, {
        order_by => "created_at asc"
    });


    my $domain = (split ":", request -> uri_base)[1];
    my $protocol = request -> secure ? "https" : "http";


    my $xml = << "SITEMAP_XML";
<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
SITEMAP_XML


    foreach my $post (@posts) {
        my $title = $post -> get_column('source');
        $xml  .= << "URL";
    <url>
        <loc>$protocol:$domain/$title</loc>
    </url>
URL
    }


    $xml .= "</urlset>";
    return $xml;
};

1;