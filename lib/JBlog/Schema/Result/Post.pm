use utf8;
package JBlog::Schema::Result::Post;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

JBlog::Schema::Result::Post

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<posts>

=cut

__PACKAGE__->table("posts");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 source

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 content

  data_type: 'mediumtext'
  is_nullable: 1

=head2 content_html

  data_type: 'mediumtext'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 0

=head2 updated_at

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "source",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "content",
  { data_type => "mediumtext", is_nullable => 1 },
  "content_html",
  { data_type => "mediumtext", is_nullable => 1 },
  "created_at",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 0,
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<post_unidx_source>

=over 4

=item * L</source>

=back

=cut

__PACKAGE__->add_unique_constraint("post_unidx_source", ["source"]);

=head1 RELATIONS

=head2 comments

Type: has_many

Related object: L<JBlog::Schema::Result::Comment>

=cut

__PACKAGE__->has_many(
  "comments",
  "JBlog::Schema::Result::Comment",
  { "foreign.post_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tags

Type: has_many

Related object: L<JBlog::Schema::Result::Tag>

=cut

__PACKAGE__->has_many(
  "tags",
  "JBlog::Schema::Result::Tag",
  { "foreign.post_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 user

Type: belongs_to

Related object: L<JBlog::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "JBlog::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-08-04 16:56:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Lx/AUNydCjOdFIKbs/QmcQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
