use strict;
use warnings;

package JBlog::Util;

use utf8;
use Exporter qw/import/;
use JBlog;
use HTML::Entities;
use HTTP::Tiny;
use JSON;


our @EXPORT_OK = qw/encode_html decode_html valid_grecaptch
    encode_password valid_password remove_special_word generate_random_string/;

sub encode_html { return encode_entities(shift); }

sub decode_html { return decode_entities(shift); }

sub valid_grecaptch {
    my ($token, $verify_url, $secret_key) = @_;

    unless($token) {
        return;
    }

    my $response = HTTP::Tiny -> new() -> post_form($verify_url, {
        secret   => $secret_key,
        response => $token
    });

    my $result = from_json($response -> {content}, {utf8 => 1});
    return $result -> {success};
}

sub encode_password {
    my ($plain_text) = @_;

    my $phrase = JBlog::passphrase($plain_text) -> generate({
        algorithm => 'Bcrypt',
        Bcrypt    => {cost => 14}
    });
    return $phrase -> rfc2307;
}

sub valid_password {
    my ($plain_text, $hashed_text) = @_;
    return JBlog::passphrase($plain_text) -> matches($hashed_text);
}

sub remove_special_word {
    my ($word) = @_;

    $word =~ s/[^0-9a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣\s]//g;
    $word =~ s/^\s+|\s+$//g;
    $word =~ s/\s+/-/g;

    return $word;
}

sub generate_random_string {
    my ($length) = @_;
    $length = 6 unless ($length || $length > 5);
    my @set = ('0' .. '9', 'a' .. 'z');
    return join '', map { $set[rand @set] } (1 .. $length);
}

1;
