use strict;
use warnings;

package JBlog::Service::User;

use utf8;
use Exporter    qw/import/;
use JBlog::Util qw/valid_password encode_password/;


our @EXPORT_OK = qw/login update/;

sub login {
    my ($schema, $email, $plain_password) = @_;

    my $user = $schema -> resultset('User') -> find({email => $email});
    unless ($user) {
        return;
    }

    my $hashed_password = $user -> get_column('pwd');
    unless (valid_password($plain_password, $hashed_password)) {
        return;
    }

    return $user;
}

sub update {
    my ($schema, %params) = @_;

    my $user = $schema -> resultset('User') -> find({email => 'whwp0913@gmail.com'});

    my $new_password = $params{pwd};
    if ($new_password) {
        $params{pwd} = encode_password($new_password);
    }

    my $tx = $schema -> txn_scope_guard;
    $user -> update(\%params);
    $tx -> commit;

    return $user -> get_columns;
}

1;
