use strict;
use warnings;

package JBlog::Service::Post;

use utf8;
use JBlog::Service::Tag;
use Exporter    qw/import/;
use JBlog::Util qw/remove_special_word encode_html generate_random_string/;


our @EXPORT_OK = qw/add update delete_by_id find_by_id find_by_title list/;

sub add {
    my ($schema, $title, $content, $content_html, $user_id, $tags) = @_;

    my $source         = _make_source_title($schema, remove_special_word($title));
    my $encode_title   = encode_html($title);
    my $encode_content = encode_html($content_html);

    my $tx   = $schema -> txn_scope_guard;
    my $post = $schema -> resultset("Post") -> create({
        title        => $encode_title,
        source       => $source,
        content      => $content,
        content_html => $encode_content,
        user_id      => $user_id
    });


    my $id = $post -> get_column("id");

    # 태그 등록
    $schema -> resultset("Tag") -> create({
        tag 	=> encode_html($_),
        post_id => $id
    }) foreach (@{$tags});

    # 신규 태그가 추가된 경우 태그 목록 갱신을 위해 업데이트 값 설정 ( 1: 업데이트 필요, 0: 필요 없음 )
    $JBlog::Service::Tag::updated = 1 if (scalar @{$tags});

    $tx  -> commit;
    return $source;
}

sub update {
    my ($schema, $post, $tags) = @_;

    my $data = {
        title        => encode_html($post -> {title}),
        content      => $post -> {content},
        content_html => encode_html($post -> {content_html})
    };

    my $old_post = $schema -> resultset("Post") -> find({
        id => $post -> {id}
    });

    # 타이틀이 변경된 경우, 중복된 개수 조사하여 치환처리
    if ($data -> {title} ne $old_post -> get_column("title")) {
        $data -> {source} = _make_source_title($schema, $post -> {title});;
    }

    my $tx = $schema -> txn_scope_guard;

    my $result = $old_post -> update($data);


    # 기존 태그가 제거된 경우 삭제
    my %update_tags      = %{$tags -> {update}};
    my @curr_tags        = $result -> tags;
    $_ -> delete foreach grep { !defined($update_tags{$_ -> get_column('id')}) } @curr_tags;


    # 신규 태그 추가
    my @add_tags = @{$tags -> {add}};
    my $post_id  = $result -> get_column("id");
    $schema -> resultset("Tag") -> create({
        tag 	=> encode_html($_),
        post_id => $post_id
    }) foreach (@add_tags);


    # 태그 변동사항 체크
    my $update_tags_size = keys %update_tags;
    my $curr_tags_size   = @curr_tags;
    my $add_tags_size    = @add_tags;
    # 신규로 추가된 태그가 있는 경우 또는 기존 태그가 삭제 된 경우는 태그 갱신
    if ($add_tags_size > 0 || ($curr_tags_size != $update_tags_size)) {
        $JBlog::Service::Tag::updated = 1;
    }

    $tx -> commit;

    return $result -> get_column('source');
}

sub delete_by_id {
    my ($schema, $id) = @_;
    my $find_post = $schema -> resultset('Post') -> find({id => $id});

    $find_post -> delete;
    $JBlog::Service::Tag::updated = 1;
}

sub find_by_id {
    my ($schema, $id) = @_;
    return $schema -> resultset("Post") -> find({id => $id});
}

sub find_by_title {
    my ($schema, $title) = @_;

    # my $find_post = $schema -> resultset("Post") -> search({
    #     'me.source' => $title}, {
    #     join       => [{'tags'}, {'comments'}],
    #     collapse   => 1,
    #     '+columns' => [
    #         (map { +{ "tags.$_" => "tags.$_" } } $schema -> source('Post') -> related_source('tags') -> columns),
    #         (map { +{ "comments.$_" => "comments.$_" } } $schema -> source('Post') -> related_source('comments') -> columns),
    #     ],
    #     order_by   => 'comments.created_at asc'
    # });

    my $find_post = $schema -> resultset("Post") -> search({
        'me.source' => $title}, {
        prefetch    => ['tags', 'comments'],
        order_by    => 'comments.created_at asc'
    });

    unless ($find_post) {
        return;
    }
    return $find_post -> first;
}

sub list {
    my ($schema, %param) = @_;

    my $keyword  = encode_html($param{q}); #search keyword
    my $tag      = encode_html($param{t}); #search tag
    my $page     = $param{page};           #current page
    my $row_size = 15;
    my @list     = ();

    if ($keyword) {
        @list = $schema -> resultset("Post") -> search({
            'me.title' => {"like", "%$keyword%"}}, {
            prefetch   => ['tags', 'comments'],
            rows       => $row_size,
            page       => $page,
            order_by   => "me.created_at desc"
        });
    }

    elsif ($tag) {
        @list = $schema -> resultset("Post") -> search({
            'tags.tag' => $tag}, {
            prefetch   => ['tags', 'comments'],
            rows       => $row_size,
            page       => $page,
            order_by   => "me.created_at desc"
        });
    }

    else {
        @list = $schema -> resultset("Post") -> search(undef, {
            prefetch => ['tags', 'comments'],
            rows     => $row_size,
            page     => $page,
            order_by => "me.created_at desc"
        });
    }

    return @list;
}

sub _make_source_title {
    my ($schema, $title) = @_;

    my $count = $schema -> resultset("Post") -> search({
        title => $title
    }) -> count;

    $title .= "-" . generate_random_string(6) if ($count);
    return $title;
}

1;
