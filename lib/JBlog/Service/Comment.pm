use strict;
use warnings;

package JBlog::Service::Comment;

use utf8;
use Exporter qw/import/;
use JBlog::Util qw/encode_html valid_grecaptch/;


our @EXPORT_OK = qw/add verify_password update get_reply/;

sub add {
    my ($schema, $data, $verify_url, $secret_key) = @_;

    my $token = $data -> {grecaptcha_token};
    unless (valid_grecaptch($token, $verify_url, $secret_key)) {
        die "[!] 올바르지 않은 접근입니다 !";
    }

    delete $data -> {grecaptcha_token};

    $data -> {comment} = encode_html($data -> {comment});
    $data -> {guest}   = encode_html($data -> {guest});

    my $parent_id = $schema -> resultset("Comment") -> create($data) -> get_column("parent_id");
    my @result    = $schema -> resultset("Comment") -> search({
        post_id   => $data -> {post_id},
        parent_id => $parent_id}, {
        order_by  => "created_at asc"
    });

    return @result;
}

sub verify_password {
    my ($schema, $data) = @_;

    my $comment_id = $data -> {id};
    my $password   = $data -> {guest_password};
    my $comment    = $schema -> resultset("Comment") -> find({id => $comment_id});

    unless ($password eq $comment -> get_column("guest_password")) {
        return;
    }
    return $comment;
}

sub update {
    my ($schema, $data, $verify_url, $secret_key) = @_;

    my $token = $data -> {grecaptcha_token};
    unless (valid_grecaptch($token, $verify_url, $secret_key)) {
        die "[!] 올바르지 않은 접근입니다 !";
    }

    delete $data -> {grecaptcha_token};

    my $comment_id     = $data -> {id};
    my $comment_text   = encode_html($data -> {comment});
    my $is_delete      = $data -> {is_delete};
    my $guest_password = $data -> {guest_password};

    my $comment = $schema -> resultset("Comment") -> find({id => $comment_id});

    unless ($comment -> get_column("guest_password") eq $guest_password) {
        die "[!] 부적절한 접근 입니다.";
    }

    my $columns = {};
    if ($is_delete eq "Y") {
        $columns -> {comment} = "삭제 된 댓글입니다.";
        $columns -> {is_delete} = "Y";
    }
    else {
        $columns -> {comment} = $comment_text;
    }
    my $result = $comment -> update($columns);
    return $result -> get_columns;
}

sub get_reply {
    my ($schema, $parent_id) = @_;

    my @list = $schema -> resultset("Comment") -> search({parent_id => $parent_id}, {
        order_by  => 'created_at asc'
    });

    my @result = ();
    foreach (@list) {
        my %reply = $_ -> get_columns;
        push @result, {
            id         => $reply{id},
            post_id    => $reply{post_id},
            parent_id  => $reply{parent_id},
            comment    => $reply{comment},
            created_at => $reply{created_at},
            guest      => $reply{guest},
            is_delete  => $reply{is_delete}
        };
    }
    return @result;
}

1;