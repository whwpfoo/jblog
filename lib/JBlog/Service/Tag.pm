use strict;
use warnings;

package JBlog::Service::Tag;

use utf8;
use Exporter qw/import/;
use Try::Tiny;


# $updated 상태 값이 '1' 인 경우 태그 목록 갱신
# 태그 추가/수정/삭제가 이루어 질 때 해당 값이 설정 됨
our $updated = 1;
our $cashed = {};
our @EXPORT_OK = qw/cashing/;

sub cashing {
    my ($schema) = @_;

    my @tags = $schema -> resultset('Tag') -> search({
        id => {'>' => 0}
    });

    # 태그 명:태그 갯수 의 맵 형태로 세팅
    undef $cashed;
    $cashed -> {$_ -> get_column('tag')}++ foreach @tags;
    $updated = 0;
}

1;
